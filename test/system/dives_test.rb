require "application_system_test_case"

class DivesTest < ApplicationSystemTestCase
  setup do
    @dife = dives(:one)
  end

  test "visiting the index" do
    visit dives_url
    assert_selector "h1", text: "Dives"
  end

  test "creating a Dive" do
    visit dives_url
    click_on "New Dive"

    fill_in "Date", with: @dife.date
    fill_in "Lat", with: @dife.lat
    fill_in "Location", with: @dife.location
    fill_in "Lon", with: @dife.lon
    fill_in "Maxdepth", with: @dife.maxDepth
    fill_in "Notes", with: @dife.notes
    fill_in "Timesecounds", with: @dife.timeSecounds
    click_on "Create Dive"

    assert_text "Dive was successfully created"
    click_on "Back"
  end

  test "updating a Dive" do
    visit dives_url
    click_on "Edit", match: :first

    fill_in "Date", with: @dife.date
    fill_in "Lat", with: @dife.lat
    fill_in "Location", with: @dife.location
    fill_in "Lon", with: @dife.lon
    fill_in "Maxdepth", with: @dife.maxDepth
    fill_in "Notes", with: @dife.notes
    fill_in "Timesecounds", with: @dife.timeSecounds
    click_on "Update Dive"

    assert_text "Dive was successfully updated"
    click_on "Back"
  end

  test "destroying a Dive" do
    visit dives_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dive was successfully destroyed"
  end
end
