require 'test_helper'

class DivesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dife = dives(:one)
  end

  test "should get index" do
    get dives_url
    assert_response :success
  end

  test "should get new" do
    get new_dife_url
    assert_response :success
  end

  test "should create dife" do
    assert_difference('Dive.count') do
      post dives_url, params: { dife: { date: @dife.date, lat: @dife.lat, location: @dife.location, lon: @dife.lon, maxDepth: @dife.maxDepth, notes: @dife.notes, timeSecounds: @dife.timeSecounds } }
    end

    assert_redirected_to dife_url(Dive.last)
  end

  test "should show dife" do
    get dife_url(@dife)
    assert_response :success
  end

  test "should get edit" do
    get edit_dife_url(@dife)
    assert_response :success
  end

  test "should update dife" do
    patch dife_url(@dife), params: { dife: { date: @dife.date, lat: @dife.lat, location: @dife.location, lon: @dife.lon, maxDepth: @dife.maxDepth, notes: @dife.notes, timeSecounds: @dife.timeSecounds } }
    assert_redirected_to dife_url(@dife)
  end

  test "should destroy dife" do
    assert_difference('Dive.count', -1) do
      delete dife_url(@dife)
    end

    assert_redirected_to dives_url
  end
end
