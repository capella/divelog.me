json.extract! dife, :id, :location, :timeSecounds, :maxDepth, :lat, :lon, :date, :notes, :created_at, :updated_at
json.url dife_url(dife, format: :json)
