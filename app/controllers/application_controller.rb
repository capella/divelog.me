class ApplicationController < ActionController::Base
  def index
    @dives = Dive.where(user: current_user).all
  end
end
