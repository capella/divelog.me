class DivesController < ApplicationController
  before_action :set_dife, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /dives
  # GET /dives.json
  def index
    @dives = Dive.where(user: current_user).all
  end

  # GET /dives/1
  # GET /dives/1.json
  def show
  end

  # GET /dives/new
  def new
    @dife = Dive.new
  end

  # GET /dives/1/edit
  def edit
  end

  # POST /dives
  # POST /dives.json
  def create
    @dife = Dive.new(dife_params)
    @dife.user = current_user

    respond_to do |format|
      if @dife.save
        format.html { redirect_to @dife, notice: 'Dive was successfully created.' }
        format.json { render :show, status: :created, location: @dife }
      else
        format.html { render :new }
        format.json { render json: @dife.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dives/1
  # PATCH/PUT /dives/1.json
  def update
    respond_to do |format|
      if @dife.update(dife_params)
        format.html { redirect_to @dife, notice: 'Dive was successfully updated.' }
        format.json { render :show, status: :ok, location: @dife }
      else
        format.html { render :edit }
        format.json { render json: @dife.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dives/1
  # DELETE /dives/1.json
  def destroy
    @dife.destroy
    respond_to do |format|
      format.html { redirect_to dives_url, notice: 'Dive was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dife
      @dife = Dive.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dife_params
      # params.require(:dife).permit(:location, :timeSecounds, :maxDepth, :lat, :lon, :date, :notes)
      params.permit(:location, :timeSecounds, :maxDepth, :lat, :lon, :date, :notes)
    end
end
