class AddUserToDive < ActiveRecord::Migration[6.0]
  def change
    add_reference :dives, :user
  end
end
