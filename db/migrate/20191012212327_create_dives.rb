class CreateDives < ActiveRecord::Migration[6.0]
  def change
    create_table :dives do |t|
      t.string :location
      t.integer :timeSecounds
      t.decimal :maxDepth
      t.decimal :lat
      t.decimal :lon
      t.datetime :date
      t.text :notes

      t.timestamps
    end
  end
end
